package br.com.mastertech.imersivo.oauth2gerenciacontato;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Oauth2GerenciaContatoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Oauth2GerenciaContatoApplication.class, args);
	}

}
