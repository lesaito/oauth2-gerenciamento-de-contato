package br.com.mastertech.imersivo.oauth2gerenciacontato;

import br.com.mastertech.imersivo.oauth2gerenciacontato.model.UsuarioPrincipalExtractor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        //http.authorizedRequests().antMatchers("/contato").authenticated(); - especifica o path autorizado
        http.authorizeRequests().anyRequest().authenticated();
    }

    @Bean
    public PrincipalExtractor principalExtractor() {
        return new UsuarioPrincipalExtractor();
    }
}
