package br.com.mastertech.imersivo.oauth2gerenciacontato.model;

import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;

import java.util.Map;

public class UsuarioPrincipalExtractor implements PrincipalExtractor {
    @Override
    public Object extractPrincipal(Map<String, Object> map) {
        Usuario usuario = new Usuario();
        usuario.setLoginId((Integer) map.get("id"));
        usuario.setNome((String) map.get("name"));

        return usuario;
    }
}

