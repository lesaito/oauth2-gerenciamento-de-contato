package br.com.mastertech.imersivo.oauth2gerenciacontato.controller;

import br.com.mastertech.imersivo.oauth2gerenciacontato.model.Usuario;
import br.com.mastertech.imersivo.oauth2gerenciacontato.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @PostMapping("/registrar")
    @ResponseStatus(HttpStatus.OK)
    public Usuario cadastrar (@AuthenticationPrincipal Usuario usuario) {
        return usuarioService.registraUsuario(usuario);
    }
}
