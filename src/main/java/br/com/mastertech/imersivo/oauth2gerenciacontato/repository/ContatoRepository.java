package br.com.mastertech.imersivo.oauth2gerenciacontato.repository;

import br.com.mastertech.imersivo.oauth2gerenciacontato.model.Contato;
import br.com.mastertech.imersivo.oauth2gerenciacontato.model.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Integer> {

    public List<Contato> findAllContatoByUsuario(Usuario usuario);

}
