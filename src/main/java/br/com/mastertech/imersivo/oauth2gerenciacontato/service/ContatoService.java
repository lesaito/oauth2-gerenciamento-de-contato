package br.com.mastertech.imersivo.oauth2gerenciacontato.service;


import br.com.mastertech.imersivo.oauth2gerenciacontato.model.Contato;
import br.com.mastertech.imersivo.oauth2gerenciacontato.model.Usuario;
import br.com.mastertech.imersivo.oauth2gerenciacontato.repository.ContatoRepository;
import br.com.mastertech.imersivo.oauth2gerenciacontato.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService {

    @Autowired
    ContatoRepository contatoRepository;

    @Autowired
    UsuarioRepository usuarioRepository;

    public Contato registraContato(Contato contato) {
        return contatoRepository.save(contato);
    }

    public List<Contato> listaContato(Usuario usuario) {
        return contatoRepository.findAllContatoByUsuario(usuario);
    }
}
