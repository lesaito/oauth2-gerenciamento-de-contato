package br.com.mastertech.imersivo.oauth2gerenciacontato.repository;

import br.com.mastertech.imersivo.oauth2gerenciacontato.model.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

    public Optional<Usuario> findUsuarioByLoginId(int loginId);
}
