package br.com.mastertech.imersivo.oauth2gerenciacontato.controller;

import br.com.mastertech.imersivo.oauth2gerenciacontato.model.Contato;
import br.com.mastertech.imersivo.oauth2gerenciacontato.model.Usuario;
import br.com.mastertech.imersivo.oauth2gerenciacontato.model.dto.ContatoRequest;
import br.com.mastertech.imersivo.oauth2gerenciacontato.service.ContatoService;
import br.com.mastertech.imersivo.oauth2gerenciacontato.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/contato")
public class ContatoController {

    @Autowired
    ContatoService contatoService;

    @Autowired
    UsuarioService usuarioService;

    @PostMapping("/registrar")
    public Contato cadastrar(@AuthenticationPrincipal Usuario usuario, @Valid @RequestBody ContatoRequest contatoRequest) {
        Usuario usuarioLogado = usuarioService.busca(usuario.getLoginId());

        Contato contato = new Contato();
        contato.setNome(contatoRequest.getNome());
        contato.setUsuario(usuarioLogado);

        return contatoService.registraContato(contato);
    }

    @GetMapping("/listar")
    public List<Contato> listar(@AuthenticationPrincipal Usuario usuario) {
        Usuario usuarioLogado = usuarioService.busca(usuario.getLoginId());

        return contatoService.listaContato(usuarioLogado);
    }


}
