package br.com.mastertech.imersivo.oauth2gerenciacontato.model;

import javax.persistence.*;

@Entity
@Table
public class Contato {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int contatoId;

    @Column
    private String nome;

    @ManyToOne
    @JoinColumn(name="usuario_id")
    private Usuario usuario;

    public int getContatoId() {
        return contatoId;
    }

    public void setContatoId(int contatoId) {
        this.contatoId = contatoId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
