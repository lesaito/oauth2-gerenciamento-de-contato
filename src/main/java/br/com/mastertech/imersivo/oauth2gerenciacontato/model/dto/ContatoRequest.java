package br.com.mastertech.imersivo.oauth2gerenciacontato.model.dto;

public class ContatoRequest {

    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
