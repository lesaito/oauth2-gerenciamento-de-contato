package br.com.mastertech.imersivo.oauth2gerenciacontato.service;

import br.com.mastertech.imersivo.oauth2gerenciacontato.model.Usuario;
import br.com.mastertech.imersivo.oauth2gerenciacontato.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    public Usuario registraUsuario(Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    public Usuario busca (int logindId) {
        Optional<Usuario> usuario = usuarioRepository.findUsuarioByLoginId(logindId);
        if (usuario.isPresent()) {
            return usuario.get();
        } else {
            return null;
        }
    }
}
